# Image picker/Cropper

Pour utiliser les deux librairies, je ne me suis servi que de la doc officielle : 'https://pub.dev/packages/image_picker' et 'https://pub.dev/packages/image_cropper'.

Le plus dur étant la mise en place de l'affichage, (il manque d'ailleurs une petite animation de chargement) mais, le reste se fait facilement, la doc montre toutes les options, tant que l'on ne se perd pas, tout va bien.
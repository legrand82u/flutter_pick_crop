import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _picker = ImagePicker();
  File _image;

//Crée un widget selon si il y a une image ou non
  Widget getImageWidget() {
    //Si il y a une image, on l'affiche
    if (_image != null) {
      return Image.file(
        _image,
        width: 250,
        height: 250,
        fit: BoxFit.cover,
      );
      //Sinon, on affiche un placeholder
    } else {
      return Image.asset(
        'assets/placeholder.jpg',
        width: 250,
        height: 250,
        fit: BoxFit.cover,
      );
    }
  }

  //Fonction pour aller cherche l'image, selon le bouton appuyé, photo ou galerie
  void getImage(String src) async {
    PickedFile tmp;
    //Prise de la photo, ou, ouverture de la galerie
    if (src == 'pic') {
      tmp = await _picker.getImage(source: ImageSource.camera);
    } else if (src == 'lib') {
      tmp = await _picker.getImage(source: ImageSource.gallery);
    }

    //Si on a une image, on demande à l'utilisateur de la rogner
    if (tmp != null) {
      File cropped = await ImageCropper.cropImage(
        sourcePath: tmp.path,
        maxWidth: 700,
        maxHeight: 700,
        compressFormat: ImageCompressFormat.jpg,
        compressQuality: 100,
        androidUiSettings: AndroidUiSettings(
          toolbarColor: Colors.blue.shade700,
          toolbarTitle: 'CropZ',
          backgroundColor: Colors.white,
          toolbarWidgetColor: Colors.blue.shade900,
        ),
      );
      //On notifie que l'image a bien été modifiée
      setState(() {
        _image = cropped;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Image Picker/Cropper'),
        ),
        body: Center(
          child: Column(
            children: <Widget>[
              Center(
                child: getImageWidget(),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  RaisedButton(
                    onPressed: () {
                      getImage('pic');
                    },
                    child: Text('Take Photo'),
                  ),
                  RaisedButton(
                    onPressed: () {
                      getImage('lib');
                    },
                    child: Text('Gallery'),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
